import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';

const SMALL_WIDTH_BREAKPOINT = 720;

const THEMES = ['default-theme', 'dark-theme', 'wurst-theme'];

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  private mediaMatcher: MediaQueryList =
    matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px`);

  users: Observable<User[]>;
  isDarkTheme = false;
  currentTheme = 'dark-theme';

  constructor(private zone: NgZone,
              private userService: UserService,
              private router: Router,
              public overlayContainer: OverlayContainer) {
    this.mediaMatcher.addListener(mql =>
      zone.run(() => this.mediaMatcher = mql));
  }

  ngOnInit() {
    this.users = this.userService.users;
    this.userService.loadAll();

    this.router.events.subscribe(() => {
      if (this.isScreenSmall()) {
        this.sidenav.close();
      }
    });
  }

  isScreenSmall(): boolean {
    return this.mediaMatcher.matches;
  }

  toggleTheme() {
    this.isDarkTheme = !this.isDarkTheme;

    const curThemeIndex = THEMES.findIndex(t => t === this.currentTheme);
    const nextThemeIndex = (curThemeIndex < THEMES.length - 1) ? curThemeIndex + 1 : 0;

    this.currentTheme = THEMES[nextThemeIndex];

    this.overlayContainer.getContainerElement().classList.add(this.currentTheme);
    // this.componentCssClass = theme;
  }

}
