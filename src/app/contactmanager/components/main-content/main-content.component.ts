import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  user: User;

  constructor(private route: ActivatedRoute,
              private userService: UserService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params['id']; // convert to number!
      if (!id) {
        id = 1;
      }

      this.user = null;

      this.userService.users.subscribe(users => {
        if (users.length === 0) { return; }

        // timeout just to show the spinner:
        setTimeout(() => {
          this.user = this.userService.userById(id);
        }, 500);

        // console.log('switch to user', id);
        // this.user = this.userService.userById(id);
      });
    });
  }
}
