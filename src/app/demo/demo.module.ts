import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoRoutingModule } from './demo-routing.module';

import { MaterialModule } from '../shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MomentUtcDateAdapter } from '../shared/moment-utc-date-adapter';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

import { FlexboxComponent } from './flexbox/flexbox.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { FormDemoComponent } from './form-demo/form-demo.component';
import { SvgTestComponent } from './svg-test/svg-test.component';
import { NfoDemoComponent } from './nfo-demo/nfo-demo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    DemoRoutingModule
  ],
  declarations: [ButtonsComponent, FormDemoComponent, FlexboxComponent, SvgTestComponent, NfoDemoComponent],
  providers: [
    // { provide: LOCALE_ID, useValue: 'de-CH' },

    // { provide: MAT_DATE_LOCALE, useValue: 'de-CH' },
    // { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_LOCALE, useValue: 'de-CH' },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: DateAdapter, useClass: MomentUtcDateAdapter },
  ]
})
export class DemoModule { }
