import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-svg-test',
  templateUrl: './svg-test.component.html',
  styleUrls: ['./svg-test.component.scss']
})
export class SvgTestComponent implements OnInit {

  // https://tympanus.net/codrops/2016/03/16/interactive-animated-svg-drum-kit/

  colorMap = new Map<string, string>();

  constructor() { }

  ngOnInit() {
    this.colorMap.set('floor-tom', 'yellow');
    this.colorMap.set('snare-drum', 'pink');
    this.colorMap.set('kick', 'green');
  }

  toggleColor(event: MouseEvent, elementName: string) {
    const colors = ['red', 'orange', 'black', 'magenta', 'cyan'];
    const curIndex = colors.findIndex(c => c === this.colorMap.get(elementName));
    const color = colors[curIndex + 1];
    this.colorMap.set(elementName, color);
  }
}
