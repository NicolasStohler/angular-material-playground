import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ButtonsComponent } from './buttons/buttons.component';
import { FormDemoComponent } from './form-demo/form-demo.component';
import { FlexboxComponent } from './flexbox/flexbox.component';
import { SvgTestComponent } from './svg-test/svg-test.component';
import { NfoDemoComponent } from './nfo-demo/nfo-demo.component';

const routes: Routes = [
  { path: 'buttons', component: ButtonsComponent },
  { path: 'form-demo', component: FormDemoComponent },
  { path: 'flexbox', component: FlexboxComponent },
  { path: 'svg-test', component: SvgTestComponent },
  { path: 'nfo-demo', component: NfoDemoComponent },
  { path: '**', redirectTo: 'form-demo' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }
