import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-demo',
  templateUrl: './form-demo.component.html',
  styles: []
})
export class FormDemoComponent implements OnInit {

  myForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      food: 'Sushi',
      // date: moment().format('YYYY-MM-DD')
      date: moment()
    });

    const formChanges$ = this.myForm.controls.date.valueChanges.subscribe((date: moment.Moment) =>
      console.log(date.format())
    );
  }

  submit() {
    console.log('submit', this.myForm.value);
    // const date = this.myForm.value.date as moment.Moment;
    console.log('date', this.myForm.value.date.format(), typeof(this.myForm.value.date));
  }

}
