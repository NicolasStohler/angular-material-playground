import { AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { TimelineMax, Expo, Elastic } from 'gsap/TweenMax';
import { trigger, state, style, transition, animate, keyframes, AnimationBuilder, sequence } from '@angular/animations';

// https://greensock.com/docs/NPMUsage
// npm problem with tree shaking: for now: include CDN script in index.html

const TOGGLE_COLORS = ['white', 'red', 'orange', 'black', 'magenta', 'cyan'];

@Component({
  selector: 'app-nfo-demo',
  templateUrl: './nfo-demo.component.html',
  styleUrls: ['./nfo-demo.component.scss'],
  // animations: [
  //   trigger('myAwesomeAnimation', [
  //     state('small', style({
  //       transform: 'scale(1)',
  //     })),
  //     state('large', style({
  //       transform: 'scale(1)',
  //     })),
  //     transition('small <=> large', animate('1000ms ease-in-out', keyframes([
  //       // style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
  //       style({opacity: 0.8, transform: 'translateY(-20px)', offset: 0.5}),
  //       //style({opacity: 1, transform: 'translateY(35px)',  offset: 0.5}),
  //       style({opacity: 1, transform: 'translateY(0)',     offset: 1.0})
  //     ]))),
  //   ]),
  // ]
})
export class NfoDemoComponent implements OnInit, AfterViewInit, OnDestroy {

  // Illustrator SVG processor:
  // https://jakearchibald.github.io/svgomg/

  // todo: animation builder
  // https://stackblitz.com/edit/angular-tnjduk?file=app/app.component.ts
  // https://stackoverflow.com/questions/46943225/using-angular-v4-animationbuilder-makes-width-unresponsive

  @ViewChild('svg') svgContainer: ElementRef;

  // @ViewChild('loadingBar')
  // public loadingBar;

  private _interactiveElements = [
    'tower', 'top', 'bottom', 'middle', 'containment',
    'rect-left', 'rect-right', 'outer', 'tower-box1', 'tower-box2',
    'donut'
  ];

  private _registeredListeners = [];

  state = 'small';

  // public percentage: number = 0;

  constructor(private elRef: ElementRef,
              private renderer: Renderer2,
              private ngZone: NgZone,
              private _builder: AnimationBuilder) { }

  ngOnInit() {
  }

  toggleColor(elementName: string, element: any) {

    console.log('click', elementName);
    const currentColor = element.getAttribute('fill');

    const curIndex = (TOGGLE_COLORS.findIndex(c => c === currentColor) % TOGGLE_COLORS.length);
    const nextIndex = (curIndex + 1) % TOGGLE_COLORS.length;
    const color =  TOGGLE_COLORS[nextIndex];

    element.setAttribute('fill', color);

    // greensock animation:
    this.ngZone.runOutsideAngular(() => {
      const timeline = new TimelineMax({
        passed: true
      });

      timeline.to(element,  0.1, {scaleX: 1.04, transformOrigin: '50% 50%', ease: Expo.easeOut})
        .to(element, 0.1, {scaleY: 0.9, transformOrigin: '50% 100%', ease: Expo.easeOut}, '0')
        // The last tween, returns the element to it's original properties
        .to(element, 0.4, {scale: 1, transformOrigin: '50% 100%', ease: Elastic.easeOut});

      // timeline.restart();
      timeline.play();
    });
  }

  ngAfterViewInit(): void {
    // ElementRef: https://stackoverflow.com/a/42692854/54159
    // renderer2: https://stackoverflow.com/a/35082441/54159

    // this.elRef.nativeElement.querySelector('button').addEventListener('click', this.onClick.bind(this));

    this._interactiveElements.forEach(el => {
      // this.elRef.nativeElement.querySelector(`#${el}`).addEventListener('click', this.toggleColor.bind(this, el));
      const element = this.svgContainer.nativeElement.querySelector(`#${el}`);
      element.setAttribute('fill', TOGGLE_COLORS[0]);
      const unregisterFn = this.renderer.listen(element, 'click', (evt) => {
        this.toggleColor(evt.target.id, evt.target);
      });
      this._registeredListeners.push(unregisterFn);
    });
  }

  unregisterEvents() {
    this._registeredListeners.forEach(fn => fn());
    this._registeredListeners = [];
  }

  testClick() {
    console.log('that was a click');
  }

  ngOnDestroy(): void {
    this.unregisterEvents();
  }

  // animateMe() {
  //   this.state = (this.state === 'small' ? 'large' : 'small');
  // }
  //
  // getPercentage() {
  //   return Math.ceil(this.percentage * 100);
  // }
  //
  // animateTo(to: number) {
  //   const loaderAnimation = this._builder.build(sequence([
  //     style({ width: '*' }),
  //     animate('350ms cubic-bezier(.35, 0, .25, 1)', style({ width: (to * 100) + '%' }))
  //   ]));
  //
  //   return loaderAnimation.create(this.loadingBar.nativeElement, {});
  // }
  //
  // upload(file: any) {
  //   this.percentage = 0;
  //   this.animateLoop(() => {
  //     console.log('anim');
  //     //this.source.next(file);
  //     // this.percentage += 0.01;
  //   });
  // }
  //
  // animateLoop(cb: Function) {
  //   if (this.percentage >= 1) {
  //     cb();
  //     return;
  //   }
  //   const player = this.animateTo(this.percentage);
  //   player.onDone(() => {
  //     // this.percentage = 100.0;
  //     // this.percentage += 0.1;
  //     this.percentage += rand(0.03, 0.08);
  //     this.animateLoop(cb);
  //   });
  //   player.play();
  // }
}

// function rand(min: number, max: number): number {
//   return (Math.random() * max) + min;
// }
