#!/bin/bash
echo publishing...

PUBLISH_DIR="//e200681-web01/c$/sites/angularmaterial"

echo building
rm -rf dist/
ng build --prod

echo copying files to server: $PUBLISH_DIR
rm -rf $PUBLISH_DIR
mkdir $PUBLISH_DIR

cp -r dist/angularmaterial/** $PUBLISH_DIR

echo done
